<?php

namespace App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Dispatcher implements RequestHandlerInterface {

    public $middlewares = [];
    private $fallbackHandler;

    public function __construct(RequestHandlerInterface $fallbackHandler) {
        $this->fallbackHandler = $fallbackHandler;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface {

        if (0 === count($this->middlewares)) {
            return $this->fallbackHandler->handle($request);
        }

        $middleware = array_shift($this->middlewares);
        return $middleware->process($request, $this);
    }

    public function pipe($middleware) {
        $this->middlewares[] = $middleware;
        return $this;
    }

}
