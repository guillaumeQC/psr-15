<?php

namespace App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DefaultHandler implements RequestHandlerInterface {

    public function handle(ServerRequestInterface $request): ResponseInterface {
        $response = new \GuzzleHttp\Psr7\Response();
        $response->getBody()->write("DEFAULT " . microtime(true));
        $response->withStatus(404);

        return $response;
    }

}
