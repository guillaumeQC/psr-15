<?php

require '../vendor/autoload.php';

use App\DefaultHandler;
use App\Dispatcher;
use App\Test1;
use App\Test2;
use GuzzleHttp\Psr7\ServerRequest;
use Middlewares\Whoops;
use function Http\Response\send;

function dump($value, $needDie = false){
    echo "<pre>";
    print_r($value);
    echo "</pre>";
    if( $needDie ) {
        die();
    }
}


$request = ServerRequest::fromGlobals();

$dispatcher = new Dispatcher((new DefaultHandler()));


$dispatcher
    ->pipe(new Whoops())
    ->pipe(new Test1())
    ->pipe(new Test2())
;

$response = $dispatcher->handle($request);


send($response);